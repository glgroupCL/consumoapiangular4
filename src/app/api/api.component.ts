import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-api',
  templateUrl: './api.component.html',
  styleUrls: ['./api.component.css']
})
export class ApiComponent implements OnInit {

  token: any;

  constructor(private http: HttpClient) {
  }

  Login(){
    let promesa = new Promise((resolve, reject) => {
      this.http.post('http://164.77.128.130:4000/auth/login', {
        "Correo": "manuel.caroca@glgroup.cl",
        "Contrasena": "123"
      },{
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        })
      }).toPromise().then(res => {
          console.log(res);
          this.token = res[0].token
          resolve(this.token)
        },err => {
          console.log("Error occured");
          reject("Error en login")
      });
    })
    return promesa;
  }

  RankingActivaciones(){

      this.http.post('http://164.77.128.130:4000/dashboard/RankingActivaciones/2', {
        "fechaRango1Inicio":"2017-07-01",
        "fechaRango1Termino":"2017-07-30",
        "fechaRango2Inicio":"2017-10-01",
        "fechaRango2Termino":"2017-10-31"
      }, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'x-api-key' : this.token

        })
      }).toPromise().then(res => {
          console.log(res);
          return res;
        },err => {
          console.log("Error occured");
          return "Error en ranking de activaciones";
      });
  
  }

  PromedioActivaciones(){
      this.http.post('http://164.77.128.130:4000/dashboard/PromedioActivacion/2', {
        "fechaRango1Inicio":"2017-11-01"
        ,"fechaRango1Termino":"2017-11-30"
        ,"fechaRango2Inicio":"2017-12-01"
        ,"fechaRango2Termino":"2017-12-31"
      }, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'x-api-key' : this.token
        })
      }).toPromise().then(res => {
          console.log(res);
          return res;
        },err => {
          console.log("Error occured");
          return "Error en promedio de activaciones";
      });
  }

  InformesAll(){
      this.http.post('http://164.77.128.130:4000/informes/All', {
          "fechainicio": "2017-07-01",
          "fechatermino": "2017-12-31",
          "periodo": 1,
          "agrupar": 0
        }, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'x-api-key' : this.token
        })
      }).toPromise().then(res => {
          console.log(res);
          return res;
        },err => {
          console.log("Error occured");
          return "Error en promedio de activaciones";
      });
  }

  async ngOnInit() {
    this.token = await this.Login();
    this.RankingActivaciones();
    this.PromedioActivaciones();
    this.InformesAll();
  }
}
